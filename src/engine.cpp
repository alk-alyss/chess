#include "engine.h"
#include <ctype.h>

using std::string;
using std::vector;

enum COLOR{black, white};

char FILES[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

Square::Square(int _rank = 0, int _file = 0): rank(_rank), file(_file), attack(false) {}

int Square::operator==(const Square& other) {
	if(this->file == other.file && this->rank == other.rank){
		return 1;
	}
	return 0;
}

Piece::Piece(const Square _place, const char _type, const bool _color):
	place(_place), type(_type), color(_color) {}

void Piece::printInfo(void){
	std::cout << "Position: " << FILES[place.file] << place.rank+1 << std::endl;
	std::cout << "Type: " << type << std::endl;
	std::cout << "Color: " << color << std::endl;
}

void Piece::move(const Square newSquare){
	this->place = newSquare;
}


Board::Board(const string fen){
	parseFen(fen);
	updateBoard();
}

void Board::parseFen(const string fen){
	vector<string> fields;
	split(fen, fields);

	vector<string> ranks;
	split(fields[0], ranks, '/');

	for(size_t i=0; i<ranks.size(); i++){
		string rank = ranks[i];
		for(size_t j=0; j<rank.size(); j++){
			if(isdigit(rank[j])){
				j+= rank[j];
				continue;
			}
			pieces.push_back(new Piece(Square(7-i, j), toupper(rank[j]), isupper(rank[j])));
		}
	}

	colorPlaying = fields[1]=="w";

	if(fields[3] != "-")
		enpassant = Square((int) (fields[3][0])-97, fields[3][1]);

	halfMoves = stoi(fields[4]);
	fullMoves = stoi(fields[5]);
}

vector<Piece*> Board::getPieces(void){
	return pieces;
}



void Board::updateBoard(void){
	std::array<std::array<Piece*, 8>, 8> pieces{};

	for(auto i: this->pieces){
		pieces[i->getFile()][i->getRank()] = i;
	}

	board = pieces;
}

std::array<std::array<Piece*, 8>, 8> Board::getBoard(void){
	return board;
}

bool Board::movePiece(Piece* piece, const Square newSquare){
	if(piece == nullptr || piece->getSquare() == newSquare) return false;

	if(board[newSquare.file][newSquare.rank]){
		if(board[newSquare.file][newSquare.rank]->getColor() == piece->getColor()) return false;
		for(auto i=pieces.begin(); i!=pieces.end(); i++){
			if(*i == board[newSquare.file][newSquare.rank]){
				delete *i;
				pieces.erase(i);
				break;
			}
		}
	}

	piece->move(newSquare);

	updateBoard();

	return true;
}

vector<Square> Board::generateMoves(Piece* piece){
	vector<Square> moves;

	if(piece == nullptr) return moves;

	if(piece->getType() == 'N') {
		for (int i=0; i<8; i++){
			Square currentSquare = piece->getSquare();

			switch (i) {
				case 1:
					currentSquare.rank += 2;
				case 0:
					currentSquare.file += 2;
					currentSquare.rank --;
					break;
				case 3:
					currentSquare.file += 2;
				case 2:
					currentSquare.rank += 2;
					currentSquare.file --;
					break;
				case 5:
					currentSquare.rank += 2;
				case 4:
					currentSquare.file -= 2;
					currentSquare.rank --;
					break;
				case 7:
					currentSquare.file += 2;
				case 6:
					currentSquare.rank -= 2;
					currentSquare.file --;
					break;
			}

			if (currentSquare.file < 0 || currentSquare.file > 7 || currentSquare.rank < 0 || currentSquare.rank > 7) continue;

			if(board[currentSquare.file][currentSquare.rank]){
				if(board[currentSquare.file][currentSquare.rank]->getColor() != piece->getColor()){
					currentSquare.attack = true;
					moves.push_back(currentSquare);
				}
				continue;
			}

			moves.push_back(currentSquare);
		}

		return moves;
	}

	for(int i=0; i<8; i++){
		if(piece->getType() == 'B' && !(i%2)) continue;
		if(piece->getType() == 'R' && i%2) continue;

		if(piece->getType() == 'P' && piece->getColor() && !(i==7 || i==0 || i==1)) continue;
		if(piece->getType() == 'P' && !piece->getColor() && !(i==5 || i==4 || i==3)) continue;

		Square currentSquare = piece->getSquare();
		for(int j=0; ;j++){
			switch(i){
				case 0:
					currentSquare.rank++;
					break;
				case 1:
					currentSquare.file++;
					currentSquare.rank++;
					break;
				case 2:
					currentSquare.file++;
					break;
				case 3:
					currentSquare.file++;
					currentSquare.rank--;
					break;
				case 4:
					currentSquare.rank--;
					break;
				case 5:
					currentSquare.file--;
					currentSquare.rank--;
					break;
				case 6:
					currentSquare.file--;
					break;
				case 7:
					currentSquare.file--;
					currentSquare.rank++;
					break;
			}

			bool pawnMove = ((piece->getRank() == 1 || piece->getRank() == 6) ? j<2 : j<1);

			bool generatingCondition = currentSquare.file>=0
			&& currentSquare.file<=7
			&& currentSquare.rank>=0
			&& currentSquare.rank<=7
			&& !(
				(piece->getType() == 'K' && j>=1)
				|| (piece->getType() == 'P' && !pawnMove)
			);

			if(!generatingCondition) break;

			if(board[currentSquare.file][currentSquare.rank]){
				if(board[currentSquare.file][currentSquare.rank]->getColor() != piece->getColor()){
					if(piece->getType() == 'P' && !(i%2)) break;

					currentSquare.attack = true;
					moves.push_back(currentSquare);
				}
				break;
			}
			if(piece->getType() == 'P' && i%2) break;
			moves.push_back(currentSquare);
		}
	}

	return moves;
}
