#include "gui.h"

using std::vector;
using std::string;

/*
	CONSTRUCTOR - DESTRUCTOR
*/

Gui::Gui(Board _board): board(_board){
	/*
	Initialise SDL components (window, renderer) + Image loading
	*/
	window = nullptr;
	
	Uint32 windowFlags = SDL_WINDOW_SHOWN;
	Uint32 rendererFlags = SDL_RENDERER_ACCELERATED;
	Uint32 imgFlags = 2;

	if(SDL_Init(SDL_INIT_VIDEO) < 0){
		throw guiException("SDL could not initialize!", 0);
	}

	window = SDL_CreateWindow("Chess Engine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, windowFlags);

	if(!window){
		throw guiException("Window could not be created!", 0);
	}

	SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

	renderer = SDL_CreateRenderer(window, -1, rendererFlags);

	if(!renderer){
		throw guiException("Failed to create renderer!", 0);
	}

	if(!(IMG_Init(imgFlags) & imgFlags)){
		throw guiException("SDL_image could not initialize!", 1);
	}

	for(int i=0; i<NUM_PIECES; i++){
		string path = "res/pieces/";
		path += std::to_string(i); 
		path += ".svg";
		textures.insert(std::pair<const char, SDL_Texture*>(PIECES_SHORT[i], loadTexture(path.c_str())));
	}

}

Gui::~Gui(void){
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	for(auto [piece, texture] : textures){
		SDL_DestroyTexture(texture);
	}
	IMG_Quit();
	SDL_Quit();
}


/*
	RENDERING TO THE SCREEN
*/

void Gui::prepareScene(SDL_Color bg){
	/*
	Clear screen and set backgroud color
	*/
	SDL_SetRenderDrawColor(renderer, bg.r, bg.b, bg.g, bg.a);
	SDL_RenderClear(renderer);
}

void Gui::presentScene(void){
	SDL_RenderPresent(renderer);
}

void Gui::fillRect(SDL_Rect rect, SDL_Color c){
	/*
	Fill the area defined by rect with color c
	*/
	SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
	SDL_RenderFillRect(renderer, &rect);
}

void Gui::outlineRect(SDL_Rect rect, SDL_Color c){
	/*
	Outline the area defined by rect with color c
	*/
	SDL_SetRenderDrawColor(renderer, c.r, c.g, c.b, c.a);
	SDL_RenderDrawRect(renderer, &rect);
}


/*
	DRAW BOARD
*/

void Gui::drawBoard(void){
	std::array<std::array<Piece*, 8>, 8> board = this->board.getBoard();

	SDL_Color dark = WHITE;
	SDL_Color light = BLACK;
	
	SDL_GetWindowSize(window, &windowSize[0], &windowSize[1]); 

	boardSize = std::min(windowSize[0], windowSize[1]);
	squareSize = boardSize / 8;
	offset = (std::max(windowSize[0], windowSize[1]) - boardSize) / 2;

	for(int i=0; i<8; i++){
		for(int j=0; j<8; j++){
			SDL_Rect squareRect = {
				j*squareSize + (WINDOW_WIDE ? offset : 0),
				(7-i)*squareSize + (WINDOW_TALL ? offset : 0),
				squareSize,
				squareSize
			};
			fillRect(squareRect, (j+i)%2 ? light : dark);

			if(board[j][i]){
				SDL_Rect pieceRect = {
					squareRect.x,
					squareRect.y,
					(int) (0.8 * squareRect.w),
					(int) (0.8 * squareRect.h)
				};
				if(board[j][i] == selectedPiece){
					continue;
				}
				if(isupper(board[j][i]->getType())){
					SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
				} else{
					SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
				}
				pieceRect.x += 0.5 * (squareRect.w-pieceRect.w);
				pieceRect.y += 0.5 * (squareRect.h-pieceRect.h);
				SDL_RenderCopy(renderer, this->textures[board[j][i]->getPiece()], NULL, &pieceRect);
			}
		}
	}

	if(!moves.empty()){
		for(auto square : moves){
			SDL_Rect squareRect = {
				square.file*squareSize + (WINDOW_WIDE ? offset : 0),
				(7-square.rank)*squareSize + (WINDOW_TALL ? offset : 0),
				squareSize,
				squareSize
			};
			
			SDL_Color moveColor = {244, 225, 14};
			if(square.attack){
				moveColor = {255, 0, 0};
			}
			
			fillRect(squareRect, moveColor);
		}
	}

	if(selectedPiece != nullptr){
		SDL_Rect pieceRect = {
			0,
			0,
			squareSize,
			squareSize
		};
		SDL_GetMouseState(&(pieceRect.x), &(pieceRect.y));
		pieceRect.x -= squareSize/2;
		pieceRect.y -= squareSize/2;
		SDL_RenderCopy(renderer, this->textures[selectedPiece->getPiece()], NULL, &pieceRect);
	}
	
}

/*
	TEXTURE LOADING
*/

SDL_Texture* Gui::loadTexture(string path){

	SDL_Texture* newTexture = nullptr;

	SDL_Surface* loadedSurface = nullptr;
	if(!(loadedSurface = IMG_Load(path.c_str()))){
		throw guiException("Failed to load image", 1);
	}

	newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
	if(!newTexture){
		throw guiException("Failed to create texture from " + path, 0);
	}

	SDL_FreeSurface(loadedSurface);

	return newTexture;
}


/*
	INPUT HANDLING
*/

void Gui::events(void){
	SDL_Event event;

	while(SDL_PollEvent(&event)){
		switch(event.type){
			case SDL_QUIT:
				RUNNING = false;
				break;
			case SDL_MOUSEBUTTONDOWN: {
				SDL_Point mouse = {event.button.x, event.button.y};
				if(WINDOW_WIDE && (mouse.x < offset || mouse.x > offset+boardSize)){
					break;
				} else if(WINDOW_TALL && (mouse.y < offset || mouse.y > offset+boardSize)){
					break;
				}

				if(WINDOW_WIDE){
					mouse.x -= offset;
				} else if(WINDOW_TALL){
					mouse.y -= offset;
				}

				Square square = screenToBoard(mouse);
				selectedPiece = this->board.getBoard()[square.file][square.rank];
				moves = this->board.generateMoves(selectedPiece);

				break;
			}
			case SDL_MOUSEBUTTONUP: {
				SDL_Point mouse = {event.button.x, event.button.y};
				if(WINDOW_WIDE && (mouse.x < offset || mouse.x > offset+boardSize)){
					break;
				} else if(WINDOW_TALL && (mouse.y < offset || mouse.y > offset+boardSize)){
					break;
				}

				if(WINDOW_WIDE){
					mouse.x -= offset;
				} else if(WINDOW_TALL){
					mouse.y -= offset;
				}

				Square square = screenToBoard(mouse);
				if(this->board.movePiece(selectedPiece, square))
					moves.clear();
				selectedPiece = nullptr;

				break;
			}
			default:
				break;
		}
	}
}

Square Gui::screenToBoard(SDL_Point point){
	return Square(7-(int) (point.y/squareSize), (int) (point.x/squareSize));
}
