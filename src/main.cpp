#include "common.h"
#include "gui.h"

SDL_Color bg = BLACK;

int main(){
	Board board = Board("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1");
	Gui* gui = nullptr;

	try{
		gui = new Gui(board);
	} catch(guiException& e) {
		std::cout << e.what();

		switch(e.getType()){
			case 0:
				std::cout << " SDL_Error: " << SDL_GetError();
				break;
			case 1:
				std::cout << " IMG_Error: " << IMG_GetError();
				break;
			default:
				break;
		}

		std::cout << std::endl;
		return 1;
	}

	while(RUNNING){
		gui->prepareScene(bg);

		gui->events();
		
		gui->drawBoard();

		gui->presentScene();

		SDL_Delay(25);
	}

	return 0;
}
