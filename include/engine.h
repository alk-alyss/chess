#ifndef ENGINE_H
#define ENGINE_H

#include "common.h"
#define NUM_PIECES 12

inline const char* PIECES[] = {"pawn", "bishop", "knight", "rook", "queen", "king"};
inline const char PIECES_SHORT[] = {'p', 'r', 'n', 'b', 'q', 'k', 'P', 'R', 'N', 'B', 'Q', 'K'};

struct Square{
	int rank;
	int file;
	bool attack;

	Square(int _rank, int _file);
	int operator==(const Square& other);
};

class Piece{
	private:
		Square place;
		char type;
		bool color;

	public:
		Piece(const Square _place, const char _type, const bool _color);
		void printInfo(void);
		inline char getPiece(void) {return color ? type : tolower(type);};
		inline char getType(void) {return type;};
		inline bool getColor(void) {return color;};
		inline int getRank(void) {return place.rank;};
		inline int getFile(void) {return place.file;};
		inline Square getSquare(void) {return place;};
		inline void move(const Square newSquare);
};

class Board{
	private:
		std::vector<Piece*> pieces;
		std::array<std::array<Piece*, 8>, 8> board;
		bool colorPlaying;
		bool castlingRights[4];
		Square enpassant;
		int halfMoves;
		int fullMoves;

		void updateBoard(void);

	public:
		Board(const std::string fen);
		void parseFen(const std::string fen);
		std::vector<Piece*> getPieces(void);
		std::array<std::array<Piece*, 8>, 8> getBoard(void);

		bool movePiece(Piece* piece, const Square newSquare);
		std::vector<Square> generateMoves(Piece* piece);
};

#endif
