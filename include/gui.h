#ifndef GUI_H
#define GUI_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "common.h"
#include "engine.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 400

#define WHITE {255, 255, 255, 255}
#define BLACK {0, 0, 0, 255}

#define WINDOW_WIDE windowSize[0]>windowSize[1]
#define WINDOW_TALL windowSize[0]<windowSize[1]

class Gui {
	private:
		SDL_Window* window;
		SDL_Renderer* renderer;
		Board board;
		std::map<const char, SDL_Texture*> textures;

		int boardSize;
		int squareSize;
		int offset;
		int windowSize[2] = {0};

		Piece* selectedPiece = nullptr;
		std::vector<Square> moves;

		Square screenToBoard(const SDL_Point point);

	public:
		Gui(const Board _board);
		~Gui(void);
		void prepareScene(const SDL_Color bg);
		void fillRect(const SDL_Rect rect, const SDL_Color c);
		void outlineRect(const SDL_Rect rect, const SDL_Color c);
		void presentScene(void);

		SDL_Texture* loadTexture(const std::string path);

		void drawBoard(void);

		void events(void);
};

class guiException : public std::exception{
	private:
		std::string msg;
		int type;

	public:
		guiException(const std::string _msg, const int _type): msg(_msg), type(_type){}
		int getType(){
			return type;
		}

	virtual const char* what() const throw()
	{
		return msg.c_str();
	}
};

#endif