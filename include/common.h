#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <map>
#include <exception>
#include <array>

#include "utils.h"

inline bool RUNNING = true;

#endif
