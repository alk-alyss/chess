# Global definitions
PROG := chess
BIN := bin
BUILD := build
SRC := src
INCLUDE := include
LIB := lib

ifeq ($(OS),Windows_NT)
	EXE := $(PROG).exe
	SRCDIRS := $(SRC)
	INCDIRS := $(INCLUDE)
	LIBDIRS := $(LIB)
else
	EXE := $(PROG)
	SRCDIRS := $(shell find $(SRC) -type d)
	INCDIRS := $(shell find $(INCLUDE) -type d)
	LIBDIRS := $(shell find $(LIB) -type d)
endif

vpath %.cpp $(SRCDIRS)
vpath %.h $(INCDIRS)

# Compiler options
WFLAGS := -Wall

# -Wl, -subsystem, windows gets rid of the console window
ifeq ($(OS), Windows_NT)
	CXXFLAGS += -Wl, -subsystem, windows
endif

CXXFLAGS += $(WFLAGS) $(addprefix -I, $(INCDIRS)) -std=c++20 $(shell sdl2-config --cflags)

# Linker options
ifeq ($(OS), Windows_NT)
	LDFLAGS += -lmingw -lSDL2main
endif

LDFLAGS += -lSDL2 -lSDL2_image -lm $(addprefix -L, $(LIBDIRS))

# Source, Object and Dependency definitions
SRCS := $(shell find $(SRC) -name *.cpp -or -name *.c)
OBJS := $(notdir $(patsubst %.cpp, %.o, $(SRCS)))
DEPS := main.cpp

# Debug build settings
DBGDIR := $(BIN)/debug
DBGEXE := $(DBGDIR)/$(EXE)
DBGOBJDIR := $(BUILD)/debug
DBGOBJS := $(addprefix $(DBGOBJDIR)/, $(OBJS))
DBGFLAGS := -g3 -O0

# Release build settings
RELDIR := $(BIN)/release
RELEXE := $(RELDIR)/$(EXE)
RELOBJDIR := $(BUILD)/release
RELOBJS := $(addprefix $(RELOBJDIR)/, $(OBJS))
RELFLAGS := -O3

.PHONY: all relrun run clean debug release remake

# Default build
all: dbgrun

dbgrun: debug
	@./$(DBGEXE)

relrun: release
	@./$(RELEXE)

# Debug rules
debug: $(DBGEXE)

$(DBGEXE): $(DBGOBJS)
	@mkdir -p $(DBGDIR)
	$(CXX) $^ $(LDFLAGS) -o $@

$(DBGOBJDIR)/%.o: %.cpp
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(DBGFLAGS) -c $< -o $@

# Release rules
release: $(RELEXE)

$(RELEXE): $(RELOBJS)
	@mkdir -p $(RELDIR)
	$(CC) $^ $(LDFLAGS) -o $@

$(RELOBJDIR)/%.o: %.cpp
	@mkdir -p $(RELOBJDIR)
	$(CC) $(CFLAGS) $(RELFLAGS) -c $< -o $@

# Clean and rebulid
remake: clean all

# Clean build files
clean:
	$(RM) -r $(BIN) $(BUILD)
